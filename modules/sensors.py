"""Get information from the arduino using the serial port"""
import serial
import configuration

def check_sensor():
    device = configuration.get_value(__file__[:-3], 'serial_device')
    """Return True if the arduino is sending any message"""
    try:
        ser = serial.Serial(device, 9600, timeout=5)
    except (OSError, serial.SerialException):
        return False

    message = ser.readline().decode("utf-8")

    if message != "":
        print(message)
        return True
    else:
        return False
