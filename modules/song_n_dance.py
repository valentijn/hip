"""If there is a signal from the arduino play a piece of media"""
# project specific
import modules.sensors
import modules.sound_file

import subprocess # run shell commands
import random
import os
import time

def run_song():
    """Play a song"""
    while True:
        if modules.sensors.check_sensor():
            file_list = os.listdir("files/music")
            random_number = random.randint(0, len(file_list) - 1)
#            subprocess.call(["omxplayer", "files/music/" + file_list[random_number]])
            subprocess.call(["mpv", "files/music/" + file_list[random_number]])
 #   else:
def run_video():
    """Play a video"""
    while True:
        if modules.sensors.check_sensor():
            file_list = os.listdir("files/video")
            random_number = random.randint(0, len(file_list) - 1)

#            subprocess.call(["omxplayer", "files/video/" + file_list[random_number]])
            subprocess.call(["mpv", "files/video/" + file_list[random_number]])

def show_image():
    while True:
        if modules.sensor.check_sensor():
            file_list = os.listdir("files/pictures")
            random_number = random.randint(0, len(file_list) - 1)

            subprocess.call("feh", "files/pictures" + file_list[random_number])

def both():
    """Play one of the two"""
    random_number = random.randint(0, 1)

    if random_number == 0:
        run_song()
    else:
        run_video()
