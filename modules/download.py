"""Download the file from a HTTPd server on the amplifier machine"""
import urllib.request

def download_file(url, the_file):
    """Download a file from the host"""
    download_directory = "files/"

    urllib.request.urlretrieve("http://" + url + "/" + the_file, download_directory + the_file)
