# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'test.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import time

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class MyEventFilter(QtCore.QObject):
    def eventFilter(self, receiver, event):
       if event.type() == QtCore.QEvent.KeyPress:
           QtGui.QMessageBox.information(None, "Filtered Key Press Event!!",
                                         "You Pressed: "+ event.text())
           return True
       elif event.type() == QtCore.QEvent.TouchBegin:
           QtGui.QMessageBox.information(None, "Test", "You touched me")
       else:
           #Call Base Class Method to Continue Normal Event Processing
           return super(MyEventFilter, self).eventFilter(receiver, event)
    
class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        QtGui.QDialog.setWindowFlags(MainWindow, QtCore.Qt.WindowStaysOnTopHint)
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.setEnabled(True)
        MainWindow.showFullScreen()
        MainWindow.setAttribute(QtCore.Qt.WA_AcceptTouchEvents)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))

        # Display a webpage
        self.webView = QtWebKit.QWebView(self.centralwidget)
        self.webView.setAttribute(QtCore.Qt.WA_AcceptTouchEvents)
        self.webView.setUrl(QtCore.QUrl(_fromUtf8("http://visithaarlem.org/sightssounds")))
        self.webView.setObjectName(_fromUtf8("webView"))
        self.horizontalLayout.addWidget(self.webView)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))

    def scroll(self):
        max_width = self.webView.page().mainFrame().scrollBarMaximum(0x1)
        print(max_width)

from PyQt4 import QtWebKit

def run():
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    MyFilter = MyEventFilter()

    app.installEventFilter(MyFilter)
    
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

