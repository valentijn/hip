"""Play a song"""
import subprocess

def play_song(path):
    """Not written; Native solution"""
    player = musicplayer.createPlayer()
    player.outSamplerate = 96000
    player.queue(path)
    player.playing = True

def shell_song(path):
    """Currently in use; Send the song to mpv"""
    subprocess.call(["mpv", "--no-osc", path])
