"""Send commands to the amplifier who, in turn, bounces them to the clusters"""
import socket
import configuration

from sys import exit
from random import randint

ZONE = configuration.get_value(__file__[:-3], 'default_zone')
COLORS = ["\033[31m", "\033[32m", "\033[33m", "\033[35m","\033[36m", "\033[92m", "\033[93m",
          "\033[94m", "\033[95m","\033[96m"]

while True:
    if ZONE == "default":
        MESSAGE = input("[\033[94m{}\033[0m]: ".format(ZONE))
    else:
        MESSAGE = input("[{0}{1}\033[0m]: ".format(COLORS[RANDOM], ZONE))

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((configuration.get_value(__file__[:-3], 'host'),
                      int(configuration.get_value(__file__[:-3], 'port'))))

        if len(MESSAGE.split()) < 1:
            continue
        if MESSAGE.split()[0] == "zone" and len(MESSAGE.split()) > 1:
            sock.sendall(bytes("zone {}".format(MESSAGE.split()[1]) + "\n", "utf-8"))
            if MESSAGE.split()[1] == ZONE:
                print("[\033[91mresponse\033[0m]: You're already on zone '{}'".format(ZONE))
            elif str(sock.recv(1232), "utf-8") == "True":
                ZONE = MESSAGE.split()[1]
                RANDOM = randint(0, 4)
            else:
                print("[\033[91mresponse\033[0m]: The zone '{}' doesn't exist".format(ZONE))
        elif MESSAGE.split()[0] == "exit":
            exit(0)
        elif MESSAGE.split()[0] == "list":
            sock.sendall(bytes("list\n", "utf-8"))
            print(str(sock.recv(1232), "utf-8"))
        else:
            sock.sendall(bytes("command {0} {1}".format(ZONE, MESSAGE) + "\n", "utf-8"))

            RECEIVED = str(sock.recv(1232), "utf-8")
            sock.close()
