"""Client control server"""
import socketserver
import socket
import sys
import configuration

#from threading import Thread

CLIENTS = {}

class TCPHandler(socketserver.StreamRequestHandler):
    """Handles the incoming requests"""
    def handle(self):
        global zone
        data = self.rfile.readline().strip()
        command = data.decode("utf-8")

        if len(command.split()) < 1:
            pass
        elif command.split()[0] == "join":
            rest = command.split()[1:]

            if len(rest) < 1:
                self.wfile.write(bytes("The amount of arguments is too small",
                                       "utf-8"))
                return

            if rest[0] not in CLIENTS:
                CLIENTS[rest[0]] = [rest[1]]
            else:
                if not rest[1] in CLIENTS[rest[0]]:
                    print("test")
                    CLIENTS[rest[0]].append(rest[1])

        elif command.split()[0] == "zone":
            if command.split()[1] in CLIENTS:
                self.wfile.write(bytes("True", "utf-8"))

        elif command.split()[0] == "list":
            clients = []

            for client in CLIENTS:
                clients.append(client)

            print(' '.join(clients))
            self.wfile.write(bytes(' '.join(clients), "utf-8"))

        elif command.split()[0] == "command":
            rest = command.split()[1:]
            section = rest[0]

            try:
                for client in CLIENTS[section]:
                    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                        try:
                            sock.connect((client, 1233))
                        except:
                            pass
                        sock.sendall(bytes(' '.join(rest[1:]) + '\n', "utf-8"))
            except KeyError:
                self.wfile.write(bytes("That zone doesn't exist.", "utf-8"))

class TCPServer(socketserver.TCPServer):
    """A custom TCP server that allows reuse of the previous port address"""
    def server_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)

SERVER = TCPServer(("", int(configuration.get_value(__file__[:-3], 'port'))), TCPHandler)

try:
    SERVER.serve_forever()
except KeyboardInterrupt:
    SERVER.server_close()
    print(CLIENTS)
    sys.exit(0)
