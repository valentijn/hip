"""Auto upload all files in the directory"""
import os
import time
import socket

def init():
    """start program"""
    old_list = os.listdir("upload")

    while True:
        file_list = os.listdir("/var/www/localhost/htdocs/upload")
        files = list(set(file_list) - set(old_list))
        old_list = file_list

        for file in files:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                sock.connect(("10.88.1.73", 1232))
                sock.sendall(bytes("download " + "upload/" + file + "\n", "utf-8"))
                sock.close()

        time.sleep(10)

init()
