"""Load settings for the various parts of the program"""
import pathlib

from configparser import SafeConfigParser
from os import getcwd, path

USER_HOME = path.expanduser("~")
CONFIG_PATHS = [
    USER_HOME + '/.config/hip/config.ini',
    USER_HOME + '/.config/hip.ini',
    USER_HOME + '/.hip/config.ini',
    USER_HOME + '/.hip.ini',
    getcwd()  + '/config.ini'
]

CONFIG = SafeConfigParser()

for configfile in CONFIG_PATHS:
    if path.exists(configfile):
        CONFIG.read(configfile)
        break

def get_value(table, row):
    """Get a value stored in the file"""
    return CONFIG[table][row]
