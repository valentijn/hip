const int ping_pin = 7; // pin to which the sensor is connected
int average;

int length_to_object();
long microseconds_to_centimeters(long microseconds);

void setup() {
    Serial.begin(9600);
    average = length_to_object();
}

void loop() {
    int cm = length_to_object();
    unsigned long time = millis();

    if (cm < average - 2 && cm != 0) {
	Serial.printf("Afval!\tcentimeters: %d\n", cm);
	delay(1500);
    }
    
    delay(100);
}

int length_to_object()
{
    long duration, cm; // long to avoid buffer overflows

	// The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
	// Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
	pinMode(ping_pin, OUTPUT);
	digitalWrite(ping_pin, LOW);
	delayMicroseconds(2);
	digitalWrite(ping_pin, HIGH);
	delayMicroseconds(5);
	digitalWrite(ping_pin, LOW);

	// the duration is the time in ms for the sound to bounce
	pinMode(ping_pin, INPUT);
	duration = pulseIn(ping_pin, HIGH); 

	// convert the time into a distance
	return microseconds_to_centimeters(duration); 
    }

    long microseconds_to_centimeters(long microseconds) {
	// The speed of sound is 340 m/s or 29 microseconds per centimeter.
	// The ping travels out and back, so to find the distance of the
	// object we take half of the distance travelled.
	return microseconds / 29 / 2;
    }
