"""HIP Client"""
import sys
import os
import configuration
from importlib import import_module

import socketserver
import socket

from multiprocessing import Process

THREADS = {}

def load_mods():
    """Load the modules in the directory"""
    mod_list = os.listdir("modules")
    mods = {}

    for mod in mod_list:
        if mod == "__pycache__" or mod[-1] == "~": #  skip cache files
            continue

        location = os.path.join("modules", mod)
        if location.endswith(".py"): # check if the file is a python source file
            size = len(mod) - 3
            name = "modules." + mod[0:size]
            mods[mod[0:size]] = import_module(name)
        else:
            print("Cannot find the file")
            continue

    return mods

class TCPHandler(socketserver.StreamRequestHandler):
    """Run the commands received from a client"""
    def handle(self):
        data = self.rfile.readline().strip()
        message = data.decode("utf-8")
        command = message.split()[0]

        if command in THREADS:
            print("nope")
            return
        elif command == "message":
            rest_of_message = ' '.join(message.split()[1:])
            THREADS[command] = Process(target=MODS["test"]\
                                       .test(rest_of_message))
        elif command == "song":
            song_name = ' '.join(message.split()[1:])
            THREADS[command] = Process(target=MODS["sound_file"].\
                                        shell_song(song_name))
        elif command == "download":
            file_name = ' '.join(message.split()[1:])
            THREADS[command] = Process(target=MODS["download"].download_file,
                   args=(self.client_address[0], file_name))
        elif command == "check":
            THREADS[command] = Process(target=MODS["sensors"].check_sensor)
        elif command == "run":
            THREADS[command] = Process(target=MODS["song_n_dance"].run_song)
        elif command == "vrun":
            THREADS[command] = Process(target=MODS["song_n_dance"].run_video)
        elif command == "both":
            THREADS[command] = Process(target=MODS["song_n_dance"].both)
        elif command == "wrun":
            THREADS[command] = Process(target=MODS["webpage"].run)
        elif command == "kill":
            THREADS[message.split()[1]].terminate()
            return
        else:
            self.wfile.write(bytes("Commando unsuccesful", "utf-8"))

        THREADS[command].start()

class TCPServer(socketserver.TCPServer):
    """A server that allows you to reuse the same port"""
    def server_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)

FILE = __file__[:-3]
MODS = load_mods()
SERVER = TCPServer((configuration.get_value(FILE, 'only_connect'),
                    int(configuration.get_value(FILE, 'server_port'))), TCPHandler)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    S2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    S2.connect(("8.8.8.8", 80))
    IP = S2.getsockname()[0]
    sock.connect(("", int(configuration.get_value(FILE, 'amp_port'))))
    sock.sendall(bytes("join default {}".format(IP), "utf-8"))

try:
    SERVER.serve_forever()
except KeyboardInterrupt:
    SERVER.server_close()
    sys.exit(0)
